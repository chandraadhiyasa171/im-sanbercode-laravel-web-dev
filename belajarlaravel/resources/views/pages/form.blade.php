<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sign Up</title>
    </head>

    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>

        <form action="/welcome" method="POST">
            @csrf
            <label>first name:</label>
            <br><br>
            <input type = "text" name = "fname">
            <br><br>

            <label>Last name:</label>
            <br><br>
            <input type = "text" name = "lname">
            <br><br>

            <label>Gender:</label>
            <br><br>
            <input type = "radio" name="gender">Male <br>
            <input type = "radio" name="gender">Female <br>
            <input type = "radio" name="gender">Other
            <br><br>

            <label>Nationally:</label>
            <select name="nationally">
                <option value = "">Indonesia</option>
                <option value = "">jepang</option>
                <option value = "">Korea</option>
            </select>
            <br><br>

            <label>Language Spoken:</label>
            <br><br>
            <input type ="checkbox" name="bahasa">Bahasa Indonesia<br>
            <input type ="checkbox" name="bahasa">English<br>
            <input type ="checkbox" name="bahasa">Other<br>
            <br><br>

            <label>Bio:</label>
            <br><br>
            <textarea rows="10" cols="30"></textarea>
            <br>

            <input type="submit" values="Sign Up">

        </form>
    </body>
</html>