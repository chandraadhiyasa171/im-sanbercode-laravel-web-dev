@extends('layouts.master')

@section('title')
    Detail Show
@endsection

@section('content')
    
<h2>Show detail index {{$cast->id}}</h2>
<br>
<p>Nama: {{$cast->nama}}</p>
<p>Umur: {{$cast->umur}}</p>
<p>Bio: {{$cast->bio}}</p>

<a href = "/cast"><button class="btn btn-primary">Back</button></a>

@endsection
