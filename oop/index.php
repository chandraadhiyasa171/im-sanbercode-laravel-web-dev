<?php
    
    require_once("animal.php");
    require_once("frog.php");
    require_once("ape.php");

    $sheep = new Animal("shaun");

    echo "Name: " . $sheep->name . "<br>" ; // "shaun"
    echo "Legs: " . $sheep->legs . "<br>"; // 4
    echo "Cold Blooded: " . $sheep->cold_blooded; // "no"

    $kodok = new Frog("buduk");
    echo "<br><br>Name: " . $kodok->name . "<br>";
    echo "Name: " . $kodok->legs . "<br>";
    echo "Cold Blooded: " . $kodok->cold_blooded . "<br>"; // "no"
    echo "Jump: " . $kodok->jump() ; // "hop hop"

    $sungokong = new Ape("kera sakti");
    echo "<br><br>Name: " . $sungokong->name . "<br>";
    echo "legs: " . $sungokong->legs . "<br>";
    echo "Cold Blooded: " . $sungokong->cold_blooded . "<br>"; // "no"
    echo "Yell: " . $sungokong->yell(); // "Auooo"

?>